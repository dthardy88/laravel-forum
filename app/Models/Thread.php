<?php

namespace App\Models;

use App\RecordsVisits;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\hasMany;
use Illuminate\Database\Eloquent\Relations\belongsTo;
use Illuminate\Database\Eloquent\Builder;
use App\Traits\RecordsActivity;
use Illuminate\Support\Str;
use Laravel\Scout\Searchable;

class Thread extends Model
{
    use HasFactory, RecordsActivity, RecordsVisits, Searchable;

    protected $with = ['owner', 'channel'];

    protected $appends = ['isSubscribedTo'];

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('replyCount', function(Builder $builder) {
            $builder->withCount('replies');
        });
        static::deleting(function($thread) {
            $thread->replies->each->delete();
        });
    }

    protected $fillable = [
    	'title',
        'body',
        'user_id',
        'channel_id',
        'slug',
        'best_reply_id',
        'locked'
    ];

    public function replies() : hasMany
    {
    	return $this->hasMany(Replies::class);
    }

    public function owner() : belongsTo
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function channel()
    {
        return $this->belongsTo(Channel::class);
    }

    public function scopeFilter(Builder $query, $filters)
    {
        return $filters->apply($query);
    }

    public function subscribe(int $userId = null)
    {
        $this->subscriptions()->create([
            'user_id' => $userId ?? auth()->user()->id
        ]);

        return $this;
    }

    public function subscriptions()
    {
        return $this->hasMany(ThreadSubscription::class);
    }

    public function unsubscribe(int $userId = null)
    {
        $this->subscriptions()
        ->where(['user_id' => $userId ?? auth()->user()->id])
        ->delete();
    }

    public function getIsSubscribedToAttribute(): bool
    {
        return auth()->user() ? $this->subscriptions()->where('user_id', auth()->user()->id)->exists() : false;
    }

    public function addReply(string $body, int $userId = null)
    {
        if($this->locked === 1) {
            throw new \Exception('Thread locked');
        } else {
            return $this->replies()->create([
                'user_id' => $userId ?? auth()->user()->id,
                'body' => $body
            ]);
        }
    }

    public function hasNewReplies(): bool
    {
        if(!auth()->user()) {
            return false;
        }
        $key = sprintf("users.%s.visits.%s", auth()->user()->id, $this->id);
        return $this->updated_at > cache($key);
    }

    public function setSlugAttribute($value): void
    {
        $slug = Str::slug($value);
        $count = 2;
        while(Thread::where('slug', $slug)->exists()) {
            $slug = Str::slug($value).'-'.$count++;
        }
        $this->attributes['slug'] = $slug;
    }

    public function getRouteKeyName(): String
    {
        return 'slug';
    }
}
