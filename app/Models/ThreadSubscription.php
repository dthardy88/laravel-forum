<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use Illuminate\Database\Eloquent\Relations\belongsTo;

class ThreadSubscription extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'thread_id'];

    public function user(): belongsTo
    {
    	return $this->belongsTo(User::class);
    }
}
