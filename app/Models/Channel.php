<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Channel extends Model
{
    use HasFactory;

    public function getRouteKeyName()
	{
	    return 'slug';
	}

	public function threads() : hasMany
	{
		return $this->hasMany(Thread::class);
	}
}
