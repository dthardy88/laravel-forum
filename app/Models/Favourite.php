<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsTo;
use Illuminate\Database\Eloquent\Relations\morphTo;
use App\Traits\RecordsActivity;

class Favourite extends Model
{
    use HasFactory, RecordsActivity;

    protected $fillable = [
    	'user_id',
    	'favourable_id',
    	'favourable_type'
    ];

    public function user() : belongsTo
    {
    	return $this->belongsTo(User::class);
    }

    public function favourited() : morphTo
    {
        return $this->morphTo('favourable');
    }
}
