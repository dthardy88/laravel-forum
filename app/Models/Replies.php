<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\belongsTo;
use App\Traits\Favourable;
use App\Traits\RecordsActivity;

class Replies extends Model
{
    use HasFactory, Favourable, RecordsActivity;

    protected $fillable = [
    	'body',
    	'user_id',
    ];

    protected $appends = ['isFavourited', 'bestReply'];

    protected $with = ['owner', 'favourites.user'];

    protected static function boot()
    {
        parent::boot();
    }

    public function owner() : belongsTo
    {
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function thread() : belongsTo
    {
        return $this->belongsTo(Thread::class);
    }

    public function wasJustPublished()
    {
        return $this->created_at->gt(Carbon::now()->subMinute());
    }

    public function mentionedUsers(String $body)
    {
        preg_match_all('/\@([\w\-]+)/', $body, $matches);
        return $matches[1];
    }

    public function setBodyAttribute($body)
    {
        $this->attributes['body'] = preg_replace('/@([\S]+)/', '<a href="/profile/$1" class="text-blue-900 hover:underline">$0</a>', $body);
    }

    public function isBestReply()
    {
        return $this->thread->fresh()->best_reply_id === $this->id;
    }

    public function getBestReplyAttribute()
    {
        return $this->isBestReply();
    }

}
