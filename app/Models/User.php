<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Relations\hasMany;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'avatar_path',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
        'email'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function threads() : hasMany
    {
        return $this->hasMany(Thread::class)->latest();
    }

    public function activity() : hasMany
    {
        return $this->hasMany(Activity::class)->latest();
    }

    public function getRouteKeyName()
    {
        return 'username';
    }

    public function lastReply()
    {
        return $this->hasOne(Replies::class)->latest('id');
    }

    public function getAvatarAttribute()
    {
        return $this->avatar_path ? asset('storage/'.$this->avatar_path) : 'https://i.pravatar.cc/100';
    }

}
