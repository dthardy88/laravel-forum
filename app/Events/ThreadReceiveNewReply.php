<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use App\Models\Thread;
use App\Models\Replies;

class ThreadReceiveNewReply
{
    use Dispatchable, SerializesModels;

    public $reply;
    public $thread;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Replies $reply, Thread $thread)
    {
        $this->reply = $reply;
        $this->thread = $thread;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
