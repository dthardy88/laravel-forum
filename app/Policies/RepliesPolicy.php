<?php

namespace App\Policies;

use App\Models\Replies;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RepliesPolicy
{
    use HandlesAuthorization;

    public function create(User $user)
    {
        if(is_null($user->fresh()->lastReply)) {
            return true;
        }
        return !$user->lastReply->wasJustPublished();
    }

    public function update(User $user, Replies $replies)
    {
        return $user->id === $replies->owner->id;
    }

    public function delete(User $user, Replies $replies)
    {
        return $user->id === $replies->owner->id;
    }


    public function setBestReply(User $user, Replies $reply)
    {
        return $reply->thread->owner->id === $user->id;
    }
}
