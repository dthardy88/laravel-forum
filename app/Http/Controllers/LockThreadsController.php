<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use Illuminate\Http\Request;

class LockThreadsController extends Controller
{
    public function __construct()
    {
        $this->middleware('isAdmin')->only('store');
    }

    public function store(Thread $thread)
    {
        $thread->update([
            'locked' => 1,
        ]);
    }

    public function destroy(Thread $thread)
    {
        $thread->update([
            'locked' => 0,
        ]);
    }
}
