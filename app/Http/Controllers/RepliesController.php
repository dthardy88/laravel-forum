<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Http\Requests\CreatePostRequest;
use http\Client\Response;
use Illuminate\Http\Request;
use App\Models\Replies;
use App\Rules\SpamFree;
use App\Models\Thread;
use App\Events\ThreadReceiveNewReply;
use Mockery\Exception;

class RepliesController extends Controller
{

    public function __construct()
    {
        $this->middleware('emailIsConfirmed')->only('store');
    }

    // No error message to state that the email is not confirmed

    public function index(Channel $channel, Thread $thread) {
        return $thread->replies()->paginate(5);
    }

    public function store(Channel $channel, Thread $thread, CreatePostRequest $request)
    {
        try {
            $reply = $thread->addReply($request->input('body'));
            event(new ThreadReceiveNewReply($reply, $thread));
            $thread->touch();
            return $reply;
        } catch(\Exception $exception) {
            return response('The thread is locked', 422);
        }

    }

    public function destroy(Replies $reply)
    {
        $this->authorize('delete', $reply);
        $status = $reply->delete();
        $message = $status ? "Reply deleted" : "Something went wrong";
        return response(['status' => $message]);
    }

    public function update(Replies $reply, Request $request)
    {
        $this->authorize('update', $reply);
        $this->validate($request, [
            'body' => ['required', 'string', new SpamFree]
        ]);
        $reply->body = $request->input('body');
        $reply->save();
    }
}
