<?php

namespace App\Http\Controllers;

use App\Models\Replies;
use App\Models\Thread;
use Illuminate\Http\Request;

class BestReplyController extends Controller
{
    public function store(Request $request, Thread $thread, Replies $reply)
    {
        $this->authorize('setBestReply', $reply);
        $thread->update([
            'best_reply_id' => $reply->id,
        ]);
    }
}
