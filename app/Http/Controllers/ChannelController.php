<?php

namespace App\Http\Controllers;

use App\Models\Channel;
use App\Models\Thread;
use App\Models\User;
use App\Trending;
use Illuminate\Database\Eloquent\Relations\HasMany;

class ChannelController extends Controller
{
    public function index(Trending $trending)
    {
        $channels = Channel::with(['threads' => function(HasMany $query) {
            $query->orderBy('created_at', 'DESC');
        }])->paginate(8);
        return view('channels.index', [
            'channels' => $channels,
            'trendingThreads' => $trending->get(),
            'threadCount' => Thread::all()->count(),
            'channelCount' => Channel::all()->count(),
            'memberCount' => User::all()->count(),
            'latestMember' => User::latest()->first()->username,
        ]);
    }

    public function show(Channel $channel, Trending $trending)
    {
        $threads = $channel->threads()->paginate(8);
        return view('threads.index', [
            'threads' => $threads,
            'trendingThreads' => $trending->get(),
        ]);
    }
}
