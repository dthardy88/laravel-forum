<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UsersController extends Controller
{
    public function show()
    {
        return User::all()->map(function ($user) {
            return [
                'key' => $user->name,
                'value' => $user->username,
            ];
        });
    }
}
