<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use App\Rules\Recaptcha;
use App\Trending;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\RedirectResponse;
use App\Filters\ThreadFilter;
use App\Models\Channel;
use Illuminate\Support\Carbon;
use App\Rules\SpamFree;

class ThreadController extends Controller
{
    public function __construct()
    {
        $this->middleware('emailIsConfirmed')->only('store');
    }

    public function index(Channel $channel = null, ThreadFilter $filters, Request $request, Trending $trending)
    {
        if($channel) {
            $query = $channel->threads()->latest();
        } else {
            $query = Thread::latest();
        }

        $threads = $query->filter($filters)->paginate(7);

        if($request->wantsJson()) {
            return $threads;
        }

        return view('threads.index', [
            'threads' => $threads,
            'trendingThreads' => $trending->get(),
        ]);
    }

    public function create()
    {
        return view('threads.create');
    }

    public function store(Request $request, Recaptcha $recaptcha) : RedirectResponse
    {
        $validDataArray = $this->validate($request, [
            'title' => ['required', 'string', new SpamFree],
            'body' => ['required', 'string', new SpamFree],
            'channel_id' => 'integer|required|exists:channels,id',
            'g-recaptcha-response' => ['required', $recaptcha],
        ]);

        $thread = Thread::create(
            array_merge(
                [
                    'user_id' => auth()->user()->id,
                    'slug' => $validDataArray['title'],
                ],
                $validDataArray
            ));

        return redirect(route('threads.show', [$thread->channel->slug, $thread->slug]))->with('flash', 'Your thread has been published!');
    }

    public function update(Request $request, Thread $thread)
    {
        $this->authorize('update', $thread);

        $validDataArray = $this->validate($request, [
            'title' => ['required', 'string', new SpamFree],
            'body' => ['required', 'string', new SpamFree],
        ]);

        $thread->update($validDataArray);
    }

    public function show(Channel $channel, Thread $thread, Trending $trending)
    {
        if(Auth::check()) {
            $key = sprintf("users.%s.visits.%s", auth()->user()->id ?? null, $thread->id);
            cache()->forever($key, Carbon::now());
        }

        $trending->push($thread);
        $thread->recordVisit();

        return view('threads.show', [
            'thread' => $thread
        ]);
    }

    public function destroy(Thread $thread) : RedirectResponse
    {
        $this->authorize('delete', $thread);
        $thread->delete();
        return redirect(route('threads.index.alias'));
    }
}
