<?php

namespace App\Http\Controllers;

use App\Models\Thread;
use App\Trending;
use Illuminate\Http\Request;
use MeiliSearch\Client;

class SearchController extends Controller
{
    public function show(Request $request, Trending $trending) {

        $client = new Client(env('MEILISEARCH_HOST'));
        $index = $client->index('threads');
        $index->updateDisplayedAttributes(['id']);
        $results = $index->search($request->query('q'))->getHits();
        $threadIds = [];
        foreach($results as $result) {
            $threadIds[] = $result['id'];
        }
        $threads = Thread::whereIn('id', $threadIds)->paginate(15);
        if($request->expectsJson()) {
            return $threads;
        }
        return view('threads.index', [
            'threads' => $threads,
            'trendingThreads' => $trending->get(),
        ]);
    }
}
