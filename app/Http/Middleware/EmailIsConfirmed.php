<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EmailIsConfirmed
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if($request->user()->email_verified_at === null) {
            return redirect(route('threads.index'))
                ->with('flash', 'You must confirm your email before you can post in this forum');
        }
        return $next($request);
    }
}
