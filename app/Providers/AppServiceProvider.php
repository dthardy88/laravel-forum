<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Models\Channel;
use Illuminate\Support\Facades\View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
//        View::composer('*', function($view) {
//            $channels = \Cache::remember('channels', 120, function() {
//                return Channel::withCount('threads')->limit(15)->get();
//            });
//            $view->with('channels', $channels);
//        });
    }
}
