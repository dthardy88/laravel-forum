<?php

namespace App;

use Illuminate\Support\Facades\Redis;

trait RecordsVisits {

    public function getVisitsAttribute(): Int
    {
        return Redis::get($this->visitKey()) ?? 0;
    }

    public function recordVisit(): void
    {
        Redis::incr($this->visitKey(), 1);
    }

    public function resetVisits()
    {
        Redis::del($this->visitKey());
    }

    public function visitKey(): String
    {
        $className = class_basename($this::class);
        return $className.'_'.$this->id.'_visits';
    }
}
