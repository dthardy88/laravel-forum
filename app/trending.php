<?php

namespace App;

use App\Models\Thread;
use Illuminate\Support\Facades\Redis;

class Trending {

    public function get(): array
    {
        return array_map('json_decode', Redis::zRevRange($this->cacheKey(), 0, 4));
    }

    public function push(Thread $thread): void
    {
        Redis::zIncrBy($this->cacheKey(), 1, json_encode($thread));
    }

    public function cacheKey(): String
    {
        return app()->environment('testing') ? 'testing_trending_threads' : 'trending_threads';
    }

}
