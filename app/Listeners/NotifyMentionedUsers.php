<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Events\ThreadReceiveNewReply;
use App\Models\User;
use App\Notifications\UserMentioned;

class NotifyMentionedUsers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ThreadReceiveNewReply $event)
    {
        User::whereIn('username', $event->reply->mentionedUsers($event->reply->body))
            ->get()
            ->each(function ($item) use ($event) {
                $item->notify(new UserMentioned($event->thread, $event->reply));
            });
    }
}
