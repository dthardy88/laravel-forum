<?php

namespace App\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use App\Models\ThreadSubscription;
use App\Notifications\ThreadWasUpdated;

class NotifySubscribers
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $event->thread->subscriptions->filter(function(ThreadSubscription $subscription) use ($event) {
            return $subscription->user_id !== $event->thread->owner->id;
        })->each(function(ThreadSubscription $subscription) use($event) {
             $subscription->user->notify(new ThreadWasUpdated($event->thread, $event->reply));
        });
    }
}
