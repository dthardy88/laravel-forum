<?php 

namespace App\Inspector;

use App\Inspector\Spam;

class KeyHeldDown
{
	public function detect(string $text)
    {
    	if(preg_match('/(.)\\1{4,}/', $text)) {
    		throw new \Exception('Your reply contains spam.');
    	}
    }

}