<?php

namespace App\Inspector;

use App\Inspector\InvalidKeywords;
use App\Inspector\KeyHeldDown;

class Spam 
{
    protected $inspectors = [
        InvalidKeywords::class,
        KeyHeldDown::class,
    ];

    public function detect(String $text) {
        foreach($this->inspectors as $inspector) {
            app($inspector)->detect($text);
        }
        return false;
    }
}