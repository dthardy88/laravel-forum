<?php

namespace App\Inspector;

use App\Inspector\Spam;
use Exception;

class InvalidKeywords
{
	protected $keywords = [
		'yahoo customer support',
	];

	public function detect(string $text)
    {
    	foreach($this->keywords as $keyword) {
    		if(stripos($text, $keyword) !== false) {
            	throw new Exception('Your reply contains spam.');
        	}
    	}
    }

}