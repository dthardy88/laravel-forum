<?php

namespace App\Traits;

use App\Models\Activity;
use Illuminate\Database\Eloquent\Relations\morphMany;

trait RecordsActivity 
{

	private static $recordEvent = ['created'];

	protected static function bootRecordsActivity() : void
	{
		foreach(static::$recordEvent as $event) {
			static::$event(function($model) use ($event) {
            	$model->recordsActivity($event);
        	});
		}
		static::deleting(function($model) {
			$model->activity()->delete();
		});
		
	}

	protected function recordsActivity($event) : void
	{
		$this->activity()->create([
            'user_id' => $this->owner->id ?? $this->user_id,
            'type' => $this->getActivityType($event),
		]);
	}

	public function activity() : morphMany
	{
		return $this->morphMany(Activity::class, 'subject');
	}

	private function getActivityType($event) : string
	{
		$type = strtolower((new \ReflectionClass($this))->getShortName());
		return "{$event}_{$type}";
	}

}