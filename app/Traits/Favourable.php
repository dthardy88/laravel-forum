<?php

namespace App\Traits;

use App\Models\Favourite;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\morphMany;

trait Favourable 
{
    protected static function bootFavourable()
    {
        static::deleting(function($model) {
            $model->favourites()->get()->each->delete();
        });
    }

	public function favourites() : morphMany
    {
    	return $this->morphMany(Favourite::class, 'favourable');
    }

    public function isFavourited() : bool
    {
        return $this->favourites->contains(function($value) {
            return $value->user->id === auth()->id();
        });
    }

    public function getIsFavouritedAttribute()
    {
        return $this->isFavourited();
    }

    public function unfavourite() : bool
    {
        return $this->favourites()->where('user_id', auth()->id())->get()->each->delete();
    }

    public function favourite() : void
    {
        $this->favourites()->firstOrCreate([
            'user_id' => auth()->id(),
        ]);
    }

}