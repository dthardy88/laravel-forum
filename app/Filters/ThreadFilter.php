<?php

namespace App\Filters;

use App\Models\User;
use App\Filters\Filter;

class ThreadFilter extends Filter {

	protected $filters = ['by', 'popular', 'unanswered'];

	/**
	* Filter a query by a given username.
	*/
	public function by($username)
	{
        $user = User::where('username', $username)->firstOrFail();
        $this->builder->where('user_id', $user->id);
	}

	/**
	* Filter a query by the replies count
	*/
	public function popular()
	{
		$this->builder->reorder('replies_count', 'desc');
	}

	/**
	* Filter a query by unanswered threads
	*/
	public function unanswered() {
		$this->builder->doesntHave('replies');
	}
}