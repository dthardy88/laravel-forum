<?php

namespace App\Filters;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

abstract class Filter {

	protected $request, $builder, $filter;

	public function __construct(Request $request)
	{
		$this->request = $request;
	}

	public function apply(Builder $builder) : Builder
	{
		$this->builder = $builder;

		collect($this->getFilters())
			->filter(function($value, $filter) {
				return method_exists($this, $filter);
			})
			->each(function($value, $filter) {
				$this->$filter($value);
			});

		return $this->builder;
	}

	public function getFilters(): array
	{
		return $this->request->only($this->filters);
	}

}
