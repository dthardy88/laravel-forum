<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Thread;
use App\Notifications\ThreadWasUpdated;

class NotificationsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function a_notification_is_created_for_replies_by_another_user()
    {

        $thread = Thread::factory()->create();

        $subscriber = User::factory()->create();
        $thread->subscribe($subscriber->id);

        $replier = User::factory()->create();

        $this->actingAs($replier);

        $this->post(route('replies.store', [$thread->channel->slug, $thread->slug]), [
            'body' => $this->faker->paragraph
        ]);

        $this->assertCount(1, $subscriber->notifications);
    }

    /** @test */
    public function a_notification_is_not_created_for_replies_by_the_thread_owner()
    {
        $thread = Thread::factory()->create();
        $this->actingAs($thread->owner);
        $thread->subscribe();

        $this->post(route('replies.store', [$thread->channel->slug, $thread->slug]), [
            'body' => $this->faker->paragraph
        ]);
        $this->assertCount(0, auth()->user()->notifications);
    }

    /** @test */
    public function a_user_can_mark_their_notifications_as_read()
    {
        $thread = Thread::factory()->create();
        $this->actingAs($thread->owner);
        $reply = $thread->addReply($this->faker->paragraph);
        $thread->owner->notify(new ThreadWasUpdated($thread, $reply));
        $notification = auth()->user()->unreadNotifications;
        $this->assertEquals($notification->first()->read_at, null);
        $this->delete(route('notification.destroy', $notification->first()->id));
        $this->assertNotEquals($notification->fresh()->first()->read_at, null);
    }

    /** @test */
    public function a_user_can_fetch_their_unread_notifications()
    {
        $thread = Thread::factory()->create();
        $this->actingAs($thread->owner);
        $reply = $thread->addReply($this->faker->paragraph);
        $thread->owner->notify(new ThreadWasUpdated($thread, $reply));
        $notifications = $this->get(route('notifications.index'));
        $this->assertEquals(json_decode(json_decode($notifications->getContent())[0]->read_at), null);
        $this->assertCount(1, json_decode($notifications->getContent()));
    }

}
