<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Thread;
use App\Models\Replies;

class ParticipateInForumTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function users_may_only_post_replies_once_per_minute()
    {
        $thread = Thread::factory()->create();
        $spamReplier = User::factory()->create();

        $this->actingAs($spamReplier);
        $reply = Replies::factory()->make();
        $this->post(route('replies.store', [$thread->channel, $thread]), [
            'body' => $reply->body
        ])
        ->assertStatus(201);

        $this->post(route('replies.store', [$thread->channel, $thread]), [
            'body' => $reply->body
        ])
        ->assertStatus(403);
    }

    /** @test */
    public function replies_that_contain_spam_may_not_be_posted()
    {
        $thread = Thread::factory()->create();

        $replier = User::factory()->create();
        $this->actingAs($replier);

        $reply = Replies::factory()->make([
            'body' => 'Yahoo Customer Support'
        ]);

        $response = $this->postJson(route('replies.store', [$thread->channel, $thread]), [
            'body' => $reply->body
        ])
        ->assertStatus(422);

    }

    /** @test */
    public function a_reply_requires_a_body()
    {
        $thread = Thread::factory()->create();

        $replier = User::factory()->create();
        $this->actingAs($replier)
        ->postJson(route('replies.store', [$thread->channel->slug, $thread->slug]), [
            'body' => null
        ])
        ->assertStatus(422);
    }

    /** @test */
    public function an_authenticated_user_may_participate_in_forum_threads()
    {
        $thread = Thread::factory()->create();
        $this->actingAs($thread->owner)
        ->postJson(route('replies.store', [$thread->channel->slug, $thread->slug]), [
            'body' => $this->faker->text
        ]);
        $this->assertCount(1, $thread->replies);
    }

    /** @test */
    public function unauthenticated_user_may_not_participate_in_forum_threads()
    {
        $thread = Thread::factory()->create();
        $this->postJson(route('replies.store', [$thread->channel->slug, $thread->slug]), [
            'body' => $this->faker->text
        ])->assertStatus(401);
    }

    /** @test */
    public function authenticated_users_can_delete_replies()
    {
        $reply = Replies::factory()->create();
        $this->actingAs($reply->owner)
            ->delete(route('replies.destroy', $reply->id));
        $this->assertEmpty($reply->fresh());
    }

    /** @test */
    public function unauthenticated_users_cannot_delete_replies()
    {
        $reply = Replies::factory()->create();
        $this->delete(route('replies.destroy', $reply->id))
            ->assertStatus(302);
    }

    /** @test */
    public function authenticated_users_can_update_replies()
    {
        $reply = Replies::factory()->create();
        $updatedReply = "The body of the reply has been changed";
        $this->actingAs($reply->owner)
            ->patch(route('replies.update', $reply->id), [
                'body' => $updatedReply
            ]);
        $this->assertEquals($reply->fresh()->body, $updatedReply);
    }

    /** @test */
    public function unauthenticated_users_cannot_update_replies()
    {
        $reply = Replies::factory()->create();
        $updatedReply = "The body of the reply has been changed";
        $this->patch(route('replies.update', $reply->id), [
                'body' => $updatedReply
            ])
            ->assertStatus(302);
        $this->assertNotEquals($reply->fresh()->body, $updatedReply);
    }

    /** @test */
    public function a_user_cannot_delete_another_users_replies()
    {
        $reply = Replies::factory()->create();
        $user = User::factory()->create();
        $response = $this->actingAs($user)
            ->delete(route('replies.destroy', $reply->id));
        $this->assertNotNull($reply->fresh());
    }

    /** @test */
    public function a_user_cannot_update_another_users_replies()
    {
        $reply = Replies::factory()->create();
        $user = User::factory()->create();
        $updatedReply = "The body of the reply has not been changed";
        $this->actingAs($user)
            ->patch(route('replies.update', $reply->id), [
                'body' => $updatedReply
            ]);
        $this->assertNotEquals($reply->fresh()->body, $updatedReply);
    }

}
