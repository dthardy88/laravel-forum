<?php

namespace Tests\Feature;

use App\Models\Replies;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class BestReplyTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->reply = Replies::factory()->create();
        $this->thread = $this->reply->thread;
    }

    /** @test */
    public function RepliesCanBeMarkedAsABestReply()
    {
        $this->assertNull($this->thread->best_reply_id);
        $this->actingAs($this->thread->owner)
            ->postJson(route('best-reply.store', [
                'thread'=> $this->thread->id,
                'reply' => $this->reply->id,
            ]));
        $this->assertTrue($this->reply->isBestReply());
    }

    /** @test */
    public function onlyTheThreadOwnerCanMarkAReplyAsABestReply()
    {
        $forumMember = User::factory()->create();
        $this->actingAs($forumMember)
            ->postJson(route('best-reply.store', [
                'thread' => $this->thread->id,
                'reply' => $this->reply->id
            ]))
            ->assertStatus(403);
    }

    /** @test */
    public function theThreadsBestReplyColumnIsUpdatedWhenABestReplyIsDeleted()
    {
        $this->thread->update([
            'best_reply_id' => $this->reply->id,
        ]);
        $this->assertNotNull($this->thread->fresh()->best_reply_id);
        $this->actingAs($this->reply->owner)
            ->delete(route('replies.destroy', ['reply' => $this->reply]));
        $this->assertNull($this->thread->fresh()->best_reply_id);
    }

}
