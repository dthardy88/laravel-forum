<?php

namespace Tests\Feature;

use App\Models\Channel;
use App\Models\Thread;
use App\Models\User;
use App\Rules\Recaptcha;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class CreateThreadsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $thread;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->thread = Thread::factory()->create();

        app()->singleton(Recaptcha::class, function() {
            return \Mockery::mock(Recaptcha::class, function($recaptcha) {
                $recaptcha->shouldReceive('passes')->andReturn(true);
            });
        });
    }

    /** @test */
    public function a_user_may_not_delete_another_users_threads()
    {
        $response = $this->actingAs($this->user)
            ->delete(route('threads.destroy', ['thread' => $this->thread], false));
        $response->assertStatus(403);
        $this->assertNotNull($this->thread->fresh());
    }

    /** @test */
    public function an_authenticated_user_can_create_new_forum_threads()
    {
        $this->actingAs($this->user)
            ->post(route('threads.store'),
                array_merge(
                    $this->thread->toArray(),
                    ['g-recaptcha-response' => 'g-recaptcha-response-wrong']
                )
            );

        $this->get(route('threads.index'))
            ->assertSee($this->thread->title)
            ->assertSee($this->thread->body);
    }

    /** @test */
    public function a_thread_must_have_the_required_fields()
    {
        $thread = Thread::factory()->make([
            'title' => null,
            'body' => null,
            'channel_id' => null
        ]);
        $this->actingAs($this->user);
        $this->post(route('threads.store', array_merge(
            $thread->toArray(),
            ['g-recaptcha-response' => 'g-recaptcha-response-wrong']
        )))
            ->assertSessionHasErrors('title')
            ->assertSessionHasErrors('body')
            ->assertSessionHasErrors('channel_id');
    }

    /** @test */
    public function authorised_users_can_delete_threads_replies_and_activities()
    {
        $thread = Thread::factory()->create([
            'title' => 'my thread title'
        ]);
        $thread->replies()->create([
            'user_id' => $thread->owner->id,
            'body' => $this->faker->text
        ]);
        $thread->replies()->create([
            'user_id' => $thread->owner->id,
            'body' => $this->faker->text
        ]);
        $this->actingAs($thread->owner)
            ->delete(route('threads.destroy', ['thread' => $thread->slug]));

        $this->assertEmpty($thread->fresh());
        $this->assertCount(0, $thread->replies);
        $this->assertCount(0, $thread->owner->activity);
    }

    /** @test */
    public function guests_cannot_delete_threads()
    {
        $this->thread->replies()->create([
            'user_id' => $this->thread->owner->id,
            'body' => $this->faker->text
        ]);
        $this->delete(route('threads.destroy', $this->thread->id))
            ->assertStatus(302);
        $this->assertNotEmpty($this->thread->fresh());
        $this->assertCount(1, $this->thread->replies);
    }

    /** @test */
    public function guests_may_not_create_threads()
    {
        $thread = Thread::factory()->make();
        $this->post(route('threads.store'), [
            array_merge(
                $thread->toArray(),
                [['g-recaptcha-response' => 'g-recaptcha-response-wrong']]
            )
        ])->assertStatus(302);
    }

    /** @test */
    public function users_must_confirm_their_email_address_to_create_posts()
    {
        $this->user->email_verified_at = null;
        $this->user->save();
        $this->actingAs($this->user)
            ->post(route('threads.store'), [
                'title' => $title = $this->faker->title,
                'body' => $body = $this->faker->paragraph,
                'channel_id' => Channel::factory()->create()->id,
                'g-recaptcha-response' => 'g-recaptcha-response-wrong'
            ])
            ->assertRedirect(route('threads.index'));

        $this->get(route('threads.index'))
            ->assertDontSee($title);
    }

    /** @test */
    public function a_thread_must_contain_a_unique_slug()
    {
        $this->actingAs($this->user);
        $this->postJson(route('threads.store'), [
            'title' => $title = $this->faker->title,
            'body' => $body = $this->faker->paragraph,
            'channel_id' => Channel::factory()->create()->id,
            'g-recaptcha-response' => 'g-recaptcha-response-wrong'
        ]);

        $threads1 = Thread::where('title', $title)->get();
        $this->assertEquals(Str::slug($title), $threads1[0]->slug);

        $this->postJson(route('threads.store'), [
            'title' => $title,
            'body' => $body = $this->faker->paragraph,
            'channel_id' => Channel::factory()->create()->id,
            'g-recaptcha-response' => 'g-recaptcha-response-wrong'
        ]);
        $threads2 = Thread::where('title', $title)->get();
        $this->assertEquals(Str::slug($title).'-2', $threads2[1]->slug);

        $this->postJson(route('threads.store'), [
            'title' => $title,
            'body' => $body = $this->faker->paragraph,
            'channel_id' => Channel::factory()->create()->id,
            'g-recaptcha-response' => 'g-recaptcha-response-wrong',
        ]);
        $threads3 = Thread::where('title', $title)->get();
        $this->assertCount(3, $threads3);
        $this->assertEquals(Str::slug($title).'-3', $threads3[2]->slug);
    }

    /** @test */
    public function a_thread_with_a_title_that_ends_in_a_number_should_generate_a_correct_slug()
    {
        $this->actingAs($this->user);
        $this->postJson(route('threads.store'), [
            'title' => $title = 'This post is funny 24',
            'body' => $body = $this->faker->paragraph,
            'channel_id' => Channel::factory()->create()->id,
            'g-recaptcha-response' => 'g-recaptcha-response-wrong'
        ]);
        $threads1 = Thread::where('title', $title)->get();
        $this->assertEquals(Str::slug($title), $threads1[0]->slug);

        $this->actingAs($this->user);
        $this->postJson(route('threads.store'), [
            'title' => $title = 'This post is funny 24',
            'body' => $body = $this->faker->paragraph,
            'channel_id' => Channel::factory()->create()->id,
            'g-recaptcha-response' => 'g-recaptcha-response-wrong'
        ]);
        $threads1 = Thread::where('title', $title)->get();
        $this->assertEquals(Str::slug($title).'-2', $threads1[1]->slug);
    }

    /** @test */
    public function thread_creation_requires_a_valid_recaptcha()
    {
        unset(app()[Recaptcha::class]);
        $this->actingAs($this->user)
            ->postJson(route('threads.store'), [
            'title' => $title = 'This post is funny 24',
            'body' => $body = $this->faker->paragraph,
            'channel_id' => Channel::factory()->create()->id,
            'g-recaptcha-response' => 'g-recaptcha-response-wrong'
        ])->assertStatus(422);
    }

}
