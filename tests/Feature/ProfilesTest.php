<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\User;
use App\Models\Thread;

class ProfilesTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function a_user_has_a_profile()
    {
        $user = User::factory()->create();
        $this->get(route('user.profile', $user->username))
            ->assertSee($user->name);
    }

    /** @test */
    public function profiles_display_all_threads_created_by_the_user()
    {
        $thread = Thread::factory()->create();
        $this->get(route('user.profile', $thread->owner->username))
            ->assertSee($thread->title)
            ->assertSee($thread->body);
    }
}
