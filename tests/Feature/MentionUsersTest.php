<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use App\Models\Thread;
use App\Models\User;

class MentionUsersTest extends TestCase
{
    use RefreshDatabase;

    /** @test */
    public function a_user_can_mention_users()
    {
        $user1 = User::factory()->create([
            'username' => 'Dboy1988'
        ]);
        $user2 = User::factory()->create([
            'username' => 'chardy'
        ]);
        $thread = Thread::factory()->create();
        $this->actingAs($user1)
            ->postJson(route('replies.store', [$thread->channel->slug, $thread->slug]), [
                'body' => '@chardy check this game out',
            ]);
        $this->assertCount(1, $user2->notifications);
    }

    /** @test */
    public function a_mention_users_list_can_be_fetched()
    {
        $user1 = User::factory()->create();
        $this->actingAs($user1);
        $response = $this->postJson(route('users.show'));
        $mentionList = json_decode($response->getContent());
        $this->assertCount(1, $mentionList);
    }
}
