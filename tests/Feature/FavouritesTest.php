<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Replies;

class FavouritesTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function guests_can_not_favourite_anything()
    {
        $reply = Replies::factory()->create();
        $this->post(route('favourites.store', $reply->id))
            ->assertStatus(302);
    }

    /** @test */
    public function an_authenticated_user_can_favourite_and_unfavourite_any_reply()
    {
        $reply = Replies::factory()->create();
        $this->actingAs($reply->owner)
            ->post(route('favourites.store', $reply->id), []);
        $this->assertCount(1, $reply->favourites);

        $this->actingAs($reply->owner)
            ->delete(route('favourites.destroy', $reply->id));
        $this->assertCount(0, $reply->fresh()->favourites);
    }

    /** @test */
    public function an_authenticated_user_may_only_favourite_a_reply_once()
    {
        $reply = Replies::factory()->create();
        $this->actingAs($reply->owner);
        $this->post(route('favourites.store', $reply->id), []);
        $this->post(route('favourites.store', $reply->id), []);
        $this->assertCount(1, $reply->favourites);
    }
}
