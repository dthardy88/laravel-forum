<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Thread;
use App\Models\Channel;
use App\Models\Replies;
use App\Models\User;

class ReadThreadsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function setUp() : void
    {
    	parent::setUp();
    	$this->thread = Thread::factory()->create();
    }

    /** @test */
    public function a_user_can_filter_threads_according_to_a_channel()
    {
        $channel = Channel::factory()->create();
        $threadInChannel = $this->thread;
        $threadInChannel->channel_id = $channel->id;
        $threadInChannel->save();
        $threadNotInChannel = Thread::factory()->create();
        $this->get(route('channel.index', $channel->slug))
            ->assertSee($threadInChannel->title)
            ->assertDontSee($threadNotInChannel->title);
    }

    /** @test */
    public function a_user_can_request_all_replies_for_a_given_thread()
    {
        $reply = Replies::factory()->create(
            ['thread_id' => $this->thread->id
        ]);
        $response = $this->get(route('replies.index', [$this->thread->channel->slug, $this->thread->slug]));

        $this->assertCount(1, $response['data']);
    }

    /** @test */
    public function a_user_can_filter_threads_by_popularity()
    {
        $threadWithTwoReplies = Thread::factory()->create();
        Replies::factory(2)->create([
            'thread_id' => $threadWithTwoReplies->id
        ]);

        $threadWithThreeReplies = Thread::factory()->create();
        Replies::factory(3)->create([
            'thread_id' => $threadWithThreeReplies->id,
        ]);

        $threadWithThreeReplies = Thread::factory()->create();
        Replies::factory(6)->create([
            'thread_id' => $threadWithThreeReplies->id,
        ]);

        $response = $this->getJson(route('threads.index.alias', ['popular' => '1']))->json();

        $this->assertEquals(6, $response['data'][0]['replies_count']);
        $this->assertEquals(3, $response['data'][1]['replies_count']);
        $this->assertEquals(2, $response['data'][2]['replies_count']);
    }

    /** @test */
    public function a_user_can_view_all_threads() : void
    {
        $this->get(route('threads.index'))
        	->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_view_a_single_thread() : void
    {
        $this->get(route('threads.show', [$this->thread->channel->slug, $this->thread->slug]))
        	->assertSee($this->thread->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_any_username()
    {
        $user = User::factory()->create([
            'name' => 'John Doe',
            'username' => 'JDoe'
        ]);
        $channel = Channel::factory()->create();
        $threadByJohn = $user->threads()->create([
            'title' => $title = $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'channel_id' => $channel->id,
            'slug' => $title,
        ]);
        $threadNotByJohn = Thread::factory()->create();
        $this->actingAs($user)
            ->get(route('threads.index', ['by' => 'JDoe']))
            ->assertSee($threadByJohn->title)
            ->assertDontSee($threadNotByJohn->title);
    }

    /** @test */
    public function a_user_can_filter_threads_by_unanswered_threads()
    {
        $threadWithReply = Thread::factory()->create();
        $threadWithReply->replies()->create([
            'user_id' => $threadWithReply->owner->id,
            'body' => $this->faker->sentence,
        ]);
        $response = $this->getJson(route('threads.index.alias', ['unanswered' => 1]));
        $this->assertCount(1, $response->getOriginalContent());
    }
}
