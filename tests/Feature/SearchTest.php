<?php

namespace Tests\Feature;

use App\Models\Thread;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SearchTest extends TestCase
{

    use RefreshDatabase, WithFaker;

    /** @test */
    public function a_user_can_search_threads()
    {
        $search = 'unique';
        config(['scout.driver' => 'meilisearch']);
        Thread::factory(2)->create();
        Thread::factory()->create([
            'title' => "A thread title with the search term $search"
        ]);

        $results = $this->getJson("/threads/search?q=$search")['data'];

        $this->assertCount(1, $results);

        Thread::latest()->take(3)->unsearchable();

    }
}
