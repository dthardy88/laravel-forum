<?php

namespace Tests\Feature;

use App\Models\Thread;
use App\Trending;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Redis;
use Tests\TestCase;

class TrendingThreadsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    public function setUp() : void
    {
        parent::setUp();
        $this->trending = new Trending();
        Redis::del($this->trending->cacheKey());
    }

    /** @test */
    public function a_trending_threads_score_is_recorded_when_visiting_a_thread()
    {
        $this->assertEmpty($this->trending->get());
        $thread = Thread::factory()->create();
        $this->get(route('threads.show', [$thread->channel, $thread]));
        $trendingThreads = $this->trending->get();
        $this->assertCount(1, $trendingThreads);
    }
}
