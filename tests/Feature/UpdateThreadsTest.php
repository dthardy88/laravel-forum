<?php

namespace Tests\Feature;

use App\Models\Thread;
use App\Models\User;
use App\Rules\Recaptcha;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UpdateThreadsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    private $user;
    private $thread;

    public function setUp(): void
    {
        parent::setUp();
        $this->user = User::factory()->create();
        $this->thread = Thread::factory()->create();

        app()->singleton(Recaptcha::class, function() {
            return \Mockery::mock(Recaptcha::class, function($recaptcha) {
                $recaptcha->shouldReceive('passes')->andReturn(true);
            });
        });
    }
    /** @test */
    public function a_thread_can_be_updated()
    {
        $this->actingAs($this->thread->owner)
            ->patchJson(route('thread.update', $this->thread), [
                'title' => $title = $this->faker->title,
                'body' => $body = $this->faker->sentence,
            ]);

        tap($this->thread->fresh(), function ($thread) use ($title, $body) {
            $this->assertEquals($title, $thread->title);
            $this->assertEquals($body, $thread->body);
        });
    }

    /** @test */
    public function a_thread_requires_a_title_and_body_to_be_updated()
    {
        $this->actingAs($this->thread->owner)
            ->patchJson(route('thread.update', $this->thread), [
            'title' => $title = $this->faker->title,
        ])->assertStatus(422);

        $this->patchJson(route('thread.update', $this->thread), [
            'body' => $title = $this->faker->sentence,
        ]);
    }

    /** @test */
    public function only_thread_owners_can_update_a_thread()
    {
        $newUser = User::factory()->create();

        $this->actingAs($newUser)
            ->patchJson(route('thread.update', $this->thread), [
                'title' => $this->faker->title,
                'body' => $this->faker->sentence,
            ])->assertStatus(403);
    }
}
