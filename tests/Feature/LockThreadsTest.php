<?php

namespace Tests\Feature;

use App\Models\Thread;
use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class LockThreadsTest extends TestCase
{
    use DatabaseMigrations, WithFaker;

    /** @test */
    public function once_locked_a_thread_may_not_receive_new_replies()
    {
        $thread = Thread::factory()->create([
            'locked' => true,
        ]);
        $this->actingAs($thread->owner)
            ->post(route('replies.store', [
                'thread' => $thread,
                'channel' => $thread->channel,
            ]), [
                'body' => $this->faker->paragraph,
            ])->assertStatus(422);
    }

    /** @test */
    public function non_administrators_may_not_locked_threads()
    {
        $thread = Thread::factory()->create();
        $this->actingAs($thread->owner)
            ->post(route('lock-thread.store', $thread))->assertStatus(403);
        $this->assertFalse(!! $thread->fresh()->locked);
    }

    /** @test */
    public function administrators_may_lock_threads()
    {
        $adminUser = User::factory()->create([
            'username' => 'DBoy1988',
        ]);
        $thread = Thread::factory()->create();
        $this->actingAs($adminUser)
            ->post(route('lock-thread.store', $thread))->assertStatus(200);
        $this->assertTrue(!! $thread->fresh()->locked);
    }

    /** @test */
    public function administrators_may_unlock_threads()
    {
        $adminUser = User::factory()->create([
            'username' => 'DBoy1988',
        ]);
        $thread = Thread::factory()->create([
            'locked' => 1,
        ]);
        $this->assertTrue(!! $thread->locked);
        $this->actingAs($adminUser)
            ->delete(route('lock-thread.destroy', $thread))
            ->assertStatus(200);
        $this->assertFalse(!! $thread->fresh()->locked);
    }
}
