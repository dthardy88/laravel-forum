<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class AddAvatarTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /** @test */
    public function onlyMembersCanAddAvatars()
    {
        $user = User::factory()->create();
        $this->postJson(route('users.avatar.store', [
            'user' => $user
        ]))->assertStatus(401);
    }

    /** @test */
    public function aValidAvatarMustBeProvided()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        $this->postJson(route('users.avatar.store', ['user' => $user]), [
            'avatar' => 'not-an-image'
        ])->assertStatus(422);
    }

    /** @test */
    public function aUserMayAddAnAvatar()
    {
        $user = User::factory()->create();
        $this->actingAs($user);
        Storage::fake('public');
         $this->postJson(route('users.avatar.store', ['user' => $user]), [
            'avatar' => $file = UploadedFile::fake()->image('avatar.jpg'),
        ]);
        Storage::disk('public')->assertExists('avatars/' . $file->hashName());
        $this->assertEquals('avatars/' . $file->hashName(), auth()->user()->avatar_path);
    }
}
