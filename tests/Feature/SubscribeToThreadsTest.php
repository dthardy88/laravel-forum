<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Thread;

class SubscribeToThreadsTest extends TestCase
{
    use WithFaker, RefreshDatabase;

    /** @test */
    public function a_user_can_subscribe_to_threads()
    {
        $thread = Thread::factory()->create();
        $this->actingAs($thread->owner);
        $this->post(route('thread.subscriptions.store', [$thread->channel->slug, $thread]));
        $this->assertCount(1, $thread->subscriptions);

        $this->post(route('replies.store', [$thread->channel->slug, $thread->id]), [
            'body' => $this->faker->paragraph
        ]);
        $this->assertCount(1, $thread->fresh()->subscriptions);
    }

    /** @test */
    public function a_user_can_unsubscribe_to_threads()
    {
        $thread = Thread::factory()->create();
        $thread->subscribe($thread->owner->id);
        $this->assertCount(1, $thread->subscriptions);
        $this->actingAs($thread->owner);
        $this->delete(route('thread.subscriptions.destroy', [$thread->channel, $thread]));
        $this->assertCount(0, $thread->fresh()->subscriptions);
    }
}
