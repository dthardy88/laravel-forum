<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Thread;
use App\Models\Channel;

class ChannelTest extends TestCase
{
	use WithFaker, RefreshDatabase;

    /** @test */
    public function a_channel_consists_of_threads()
    {
        $channel = Channel::factory()->create();
        $thread = Thread::factory()->create([
        	'channel_id' => $channel->id
        ]);
        $this->assertTrue($channel->threads->contains($thread));
    }
}
