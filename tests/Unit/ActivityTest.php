<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\Thread;
use App\Models\Replies;
use App\Models\Activity;

class ActivityTest extends TestCase
{
	use WithFaker, RefreshDatabase;

    /** @test */
    public function activity_is_recorded_when_a_thread_is_created()
    {
        $thread = Thread::factory()->create();
        $this->assertDatabaseHas('activities', [
        	'type' => 'created_thread',
        	'user_id' => $thread->owner->id,
        	'subject_id' => $thread->id,
        	'subject_type' => Thread::class
        ]);
    }

    /** @test */
    public function activity_is_recorded_when_a_reply_is_created()
    {
    	$reply = Replies::factory()->create();
    	$this->assertDatabaseHas('activities', [
        	'type' => 'created_replies',
        	'user_id' => $reply->owner->id,
        	'subject_id' => $reply->id,
        	'subject_type' => Replies::class
        ]);
        $this->assertEquals(2, Activity::count());
    }
}
