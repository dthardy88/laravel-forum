<?php

namespace Tests\Unit;

use Carbon\Carbon;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;

use App\Models\Replies;

class ReplyTest extends TestCase
{
	use RefreshDatabase;

    /** @test */
    public function mentioned_usernames_are_wrapped_in_anchor_tags()
    {
        $reply = Replies::factory()->create([
            'body' => 'Hello @JaneDoe'
        ]);
        $this->assertEquals(
            'Hello <a href="/profile/JaneDoe" class="text-blue-900 hover:underline">@JaneDoe</a>',
            $reply->body
        );
    }

    /** @test */
    public function it_has_an_owner()
    {
        $reply = Replies::factory()->create();
        $this->assertInstanceOf(User::class, $reply->owner);
    }

    /** @test */
    public function it_knows_if_it_was_just_published()
    {
        $reply = Replies::factory()->create();
        $this->assertTrue($reply->wasJustPublished());
        $reply->created_at = Carbon::now()->subMonth();
        $this->assertfalse($reply->wasJustPublished());
    }

    /** @test */
    public function it_can_detect_all_mentioned_users_in_the_body()
    {
        $reply = Replies::factory()->create([
            'body' => '@JaneDoe wants to talk to @JohnDoe'
        ]);
        $this->assertEquals(['JaneDoe', 'JohnDoe'], $reply->mentionedUsers($reply->body));
    }

    /** @test */
    public function aReplyIncludesABestReplyProperty()
    {
        $reply = Replies::factory()->create();
        $this->assertFalse($reply->isBestReply());
        $reply->thread->update([
            'best_reply_id' => $reply->id,
        ]);
        $this->assertTrue($reply->isBestReply());
    }

}
