<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use App\Models\User;
use App\Models\Thread;

class UserTest extends TestCase
{
	use WithFaker, RefreshDatabase;

    /** @test */
    public function a_user_can_fetch_their_most_recent_reply()
    {
        $thread = Thread::factory()->create();
        $replier = User::factory()->create();
        $this->actingAs($replier);
        $reply1 = $thread->addReply($this->faker->paragraph);
        $reply2 = $thread->addReply($this->faker->paragraph);
        $this->assertEquals($reply2->id, $replier->lastReply->id);
    }

    /** @test */
    public function a_user_has_an_avatar_path()
    {
        $user1 = User::factory()->create([
            'avatar_path' => 'avatar.jpg'
        ]);
        $this->assertEquals('http://laravel-forum.test/storage/avatar.jpg', $user1->avatar);
        $user2 = User::factory()->create();
        $this->assertEquals('https://i.pravatar.cc/100', $user2->avatar);
    }
}
