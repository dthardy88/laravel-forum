<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Models\Thread;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\User;
use App\Models\Channel;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Carbon;

class ThreadTest extends TestCase
{
	use RefreshDatabase, WithFaker;

    private $thread;

    protected function setUp(): void
    {
        parent::setUp();
        $this->thread = Thread::factory()->create();
    }

    /** @test */
    public function a_thread_has_replies()
    {
        $this->assertInstanceOf('Illuminate\Database\Eloquent\Collection', $this->thread->replies);
    }

    /** @test */
    public function a_thread_has_a_creator()
    {
        $this->assertInstanceOf(User::class, $this->thread->owner);
    }

    /** @test */
    public function a_thread_can_have_a_reply()
    {
    	$reply = $this->thread->replies()->create([
    		'body' => $this->faker->text,
            'user_id' => $this->thread->owner->id,
    	]);
        $this->assertCount(1, $this->thread->replies);
    }

    /** @test */
    public function a_thread_belongs_to_a_channel()
    {
        $this->assertInstanceOf(Channel::class, $this->thread->channel);
    }

    /** @test */
    public function a_thread_can_be_subscribed_to()
    {
        $this->actingAs($this->thread->owner);
        $this->thread->subscribe();
        $this->assertCount(1, $this->thread->subscriptions()->where('user_id', auth()->user()->id)->get());
    }

    /** @test */
    public function a_thread_can_be_unsubscribed_to()
    {
        $this->actingAs(User::factory()->create());
        $this->thread->subscribe(auth()->user()->id);
        $this->thread->unsubscribe();
        $this->assertCount(0, $this->thread->subscriptions()->where('user_id', auth()->user()->id)->get());
    }

    /** @test */
    public function a_thread_contains_an_isSubscribedTo_attribute()
    {
        $this->actingAs(User::factory()->create());
        $this->thread->subscribe(auth()->user()->id);
        $this->assertTrue($this->thread->isSubscribedTo);
    }

    /** @test */
    public function a_thread_title_is_bold_if_there_are_unread_replies()
    {
        $this->actingAs(User::factory()->create());
        $this->assertTrue($this->thread->hasNewReplies());
        $key = sprintf("users.%s.visits.%s", auth()->user()->id, $this->thread->id);
        cache()->forever($key, Carbon::now());
        $this->assertFalse($this->thread->hasNewReplies());
    }

    /** @test */
    public function a_thread_visit_count_is_recorded()
    {
        $this->thread->resetVisits();
        $this->assertEquals(0, $this->thread->visits);
        $this->thread->recordVisit();
        $this->assertEquals(1, $this->thread->visits);
    }
}
