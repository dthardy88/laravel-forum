<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use PHPUnit\Framework\TestCase;
use App\Inspector\Spam;

class SpamTest extends TestCase
{
    use RefreshDatabase;
	public function setUp(): void
	{
		parent::setUp();
		$this->spam = new Spam();
	}

    /** @test */
    public function it_checks_for_invalid_keywords()
    {
        $this->assertFalse($this->spam->detect('Text with no spam'));
        $this->expectException('Exception');
        $this->spam->detect('Yahoo customer support');
    }

    /** @test */
    public function it_checks_for_any_key_being_held_down()
    {
    	$this->expectException('Exception');
    	$this->assertFalse($this->spam->detect('Hello world aaaaaa'));
    }

}
