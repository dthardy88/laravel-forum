<?php

use App\Http\Controllers\BestReplyController;
use App\Http\Controllers\ChannelController;
use App\Http\Controllers\LockThreadsController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\UserAvatarController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ThreadController;
use App\Http\Controllers\RepliesController;
use App\Http\Controllers\FavouritesController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ThreadSubscriptionsController;
use App\Http\Controllers\NotificationController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware('auth')->group(function()
{
	Route::get('/thread/create', [ThreadController::class, 'create'])
        ->name('threads.create');
	Route::post('/threads', [ThreadController::class, 'store'])
        ->name('threads.store');
    Route::patch('/thread/{thread}/update', [ThreadController::class, 'update'])
        ->name('thread.update');
    Route::delete('/threads/{thread}/destroy', [ThreadController::class, 'destroy'])
        ->name('threads.destroy');

    Route::post('/lock-thread/{thread}', [LockThreadsController::class, 'store'])->name('lock-thread.store');
    Route::delete('/lock-thread/{thread}', [LockThreadsController::class, 'destroy'])->name('lock-thread.destroy');

	Route::post('/threads/{channel}/{thread}/replies', [RepliesController::class, 'store'])
        ->name('replies.store');
	Route::delete('/replies/{reply}/destroy', [RepliesController::class, 'destroy'])
        ->name('replies.destroy');
	Route::patch('/replies/{reply}/update', [RepliesController::class, 'update'])
        ->name('replies.update');

    Route::post('{thread:id}/replies/{reply}/best', [BestReplyController::class, 'store'])
        ->name('best-reply.store');

	Route::post('/replies/{reply}/favour', [FavouritesController::class, 'store'])
        ->name('favourites.store');
	Route::delete('/replies/{reply}/unfavour', [FavouritesController::class, 'destroy'])
        ->name('favourites.destroy');


	Route::post('/threads/{channel}/{thread}/subscribe', [ThreadSubscriptionsController::class, 'store'])
        ->name('thread.subscriptions.store');
	Route::delete('/threads/{channel}/{thread}/subscribe', [ThreadSubscriptionsController::class, 'destroy'])
        ->name('thread.subscriptions.destroy');

	Route::get('/profile/notifications/', [NotificationController::class, 'index'])
        ->name('notifications.index');
	Route::delete('/profile/notifications/{notification}', [NotificationController::class, 'destroy'])
        ->name('notification.destroy');

    Route::post('/users', [UsersController::class, 'show'])
        ->name('users.show');

    Route::post('/users/{user}/avatar', [UserAvatarController::class, 'store'])
        ->name('users.avatar.store');

});


Route::get('/', [ChannelController::class, 'index'])
	->name('channels.index');
Route::get('/channel/{channel}', [ChannelController::class, 'show'])
    ->name('channel.show');

Route::get('/threads/search', [SearchController::class, 'show'])
    ->name('search.show');
Route::get('/threads', [ThreadController::class, 'index'])
	->name('threads.index.alias');
Route::get('/threads/{channel}', [ThreadController::class, 'index'])
	->name('channel.index');
Route::get('/threads/{channel}/{thread}', [ThreadController::class, 'show'])
	->name('threads.show');
Route::get('/profile/{user}', [ProfileController::class, 'show'])
	->name('user.profile');
Route::get('/threads/{channel}/{thread}/replies', [RepliesController::class, 'index'])->name('replies.index');

require __DIR__.'/auth.php';
