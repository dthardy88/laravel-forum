<?php

namespace Database\Factories;

use App\Models\Thread;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;
use App\Models\Channel;

class ThreadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Thread::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user = User::factory()->create();
        $channel = Channel::factory()->create();
        return [
            'user_id' => $user->id,
            'channel_id' => $channel->id,
            'title' => $title = $this->faker->sentence,
            'body' => $this->faker->paragraph,
            'slug' => $title,
            'locked' => false,
        ];
    }
}
