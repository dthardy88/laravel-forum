<?php

namespace Database\Factories;

use App\Models\Replies;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Thread;
use App\Models\User;

class RepliesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Replies::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'thread_id' => function() {
                return Thread::factory()->create()->id;
            },
            'user_id' => function() {
                return User::factory()->create()->id;
            },
            'body' => $this->faker->paragraph,
        ];
    }
}
