const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],
    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
                openSans: ['Open Sans', 'sans-serif;'],
            },
            fontSize: {
                s: ['0.5rem', { lineHeight: '0.8rem' }],
            },
        },
        colors: {
            primary: {
                100: '#43a6df',
            },
            secondary: {
                100: '#2e3539',
            },
            white: {
                100: '#ffffff',
                200: '#f6f6f6',
            },
            gray: {
                100: '#333',
                300: '#b3b3b3',
                400: '#777',
                500: '#424242',
            },
            transparent: 'transparent',
        },
        borderColor: {
            gray: {
                100: '#e4e4e4',
            }
        },
        borderWidth: {
            1: '1px',
        },
        screens: {
            'xs': '320px',
            // => @media (min-width: 320px) { ... }
            'sm': '640px',
            // => @media (min-width: 640px) { ... }
            'md': '768px',
            // => @media (min-width: 768px) { ... }
            'lg': '1024px',
            // => @media (min-width: 1024px) { ... }
            'xl': '1280px',
            // => @media (min-width: 1280px) { ... }
            '2xl': '1536px',
            // => @media (min-width: 1536px) { ... }
        },
    },
    variants: {
        extend: {
            opacity: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms')],
};
