@extends('app')
@section('content')
	<div class="w-3/4">
        @can('update', $user)
            <avatarForm
                :user="{{$user}}"
                avatar="{{$user->avatar}}">
            </avatarForm>
            <hr />
        @endcan
		<div class="threads mt-4">
			<h2 class="text-2xl font-bold pb-2">{{$user->name}} Threads</h2>
			@forelse($activities as $date => $activity)
				<h3 class="pb-4 pt-8 font-bold">{{$date}}</h3>
				@foreach($activity as $record)
					@if(view()->exists("activities.{$record->type}"))
						@include("activities.{$record->type}", ['activity' => $record])
					@endif
				@endforeach
				@empty
				<p>No activity to show for this user</p>
			@endforelse
		</div>
	</div>
	<div class="w-1/4" id="sidebar">
	</div>
@endsection


