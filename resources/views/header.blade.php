<header>
    <div class="container mx-auto pt-4 pb-0 max-w-6xl px-2 mb-2">
        <div class="sm:flex mb-4">
            <div class="flex-1 flex">
                @include('components.search-bar')
            </div>
            <div class="flex-1">
                @include('components.login')
            </div>
        </div>
        <div class="intro w-full bg-primary-100 max-w-7xl mx-auto pt-16 pr-8 pb-2 pl-2 text-white-100 text-4xl">
            <h1 class="font-light text-white-100">Laravel Forum</h1>
        </div>
        <div class="breadcrumbs bg-secondary-100 w-full text-white-100 mt-2 p-2">
            {{request()->path()}}
        </div>
    </div>
</header>
