@component('components.activities.activities')
	@slot('heading')
		<a
			class="text-blue-900 hover:underline"
			href="{{route('threads.show', [$activity->subject->favourited->thread->channel, $activity->subject->favourited->thread->id]).'#reply-'.$activity->subject->favourited->thread->id}}">
			{{$user->name}} favourited a reply
		</a>
	@endslot
	@slot('date')
		{{$activity->created_at->diffForHumans()}}
	@endslot
	@slot('body')
		{{$activity->subject->favourited->body}}
	@endslot
@endcomponent

