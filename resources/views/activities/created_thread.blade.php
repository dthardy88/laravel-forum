@component('components.activities.activities')
	@slot('heading')
		{{$activity->subject->owner->name}} published a thread "<a href="{{route('threads.show', [$activity->subject->channel->slug, $activity->subject->slug])}}" class="text-blue-900 hover:underline">{{$activity->subject->title}}</a>"
	@endslot
	@slot('date')
		{{$activity->created_at->diffForHumans()}}
	@endslot
	@slot('body')
		{{$activity->subject->body}}
	@endslot
@endcomponent
