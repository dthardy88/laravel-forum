<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel Forum') }}</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">
        <link rel="stylesheet" href="{{ asset('css/fontawesome.css') }}">
        <script src="https://www.google.com/recaptcha/api.js" async defer></script>
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://kit.fontawesome.com/e11e7f0ba3.js" crossorigin="anonymous"></script>
    </head>
    <div id="app">
        @include('header')
        <body>
            <div class="container mx-auto max-w-6xl px-2">
                @yield('content')
                <flash message="{{session('flash')}}"></flash>
            </div>
        </body>
        @include('footer')
    </div>
</html>

<script>
    window.App = {!! json_encode([
        'signedIn' => auth::check(),
        'user' => auth::user(),
        ]) !!};
</script>

<style>
    [v-cloak]: {display:none;}
</style>
