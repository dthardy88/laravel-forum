<x-auth-validation-errors class="mb-4" :errors="$errors"></x-auth-validation-errors>
<form
    id="login-form"
    method="POST"
    action="{{ route('login') }}">
    @csrf
    <h1 class="text-primary-100 pb-6">Login</h1>
    <div>
        <x-label for="email" :value="__('Email')"></x-label>
        <x-input
            id="email"
            class="block mt-1 email-field"
            type="email"
            name="email"
            :value="old('email')"
            required autofocus>
        </x-input>
    </div>
    <div class="mt-4">
        <x-label for="password" :value="__('Password')"></x-label>
        <x-input
            id="password"
            class="block mt-1"
            type="password"
            name="password"
            required autocomplete="current-password">
        </x-input>
    </div>
    <div class="flex items-center mt-4">
        <x-button>
            {{ __('Login') }}
        </x-button>
        <div class="flex-1 flex justify-end">
            <label for="remember_me" class="inline-flex items-center">
                <input
                    id="remember_me"
                    type="checkbox"
                    class="border-gray-100 cursor-pointer"
                    name="remember">
                <span class="ml-2 text-sm text-gray-600">
                            {{ __('Remember me') }}
                        </span>
            </label>
        </div>
    </div>
</form>
