<div id="statstics-widget" class="mt-4 mb-4">
    <div class="bg-secondary-100">
        <h3 class="text-white-100 p-4">Statistics</h3>
    </div>
    <div class="pb-4 bg-white-100 pt-4 pr-4 border-1 border-solid border-gray-100">
        <ul class="pl-8 flex mb-8 text-gray-100">
            <li class="list-disc">Total threads <span class="font-semibold">{{$threadCount}}</span></li>
            <li class="ml-6 list-disc">Total channels <span class="font-semibold">{{$channelCount}}</span></li>
            <li class="ml-6 list-disc">Total members <span class="font-semibold">{{$memberCount}}</span></li>
            <li class="ml-6 list-disc">Our newest member <span class="font-semibold">{{$latestMember}}</span></li>
        </ul>
    </div>
</div>

