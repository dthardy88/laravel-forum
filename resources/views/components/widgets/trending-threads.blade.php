@if(count($trendingThreads))
    <div class="mt-4">
        <div class="bg-secondary-100">
            <h3 class="text-white-100 p-4">Trending Threads</h3>
        </div>
        <div class="pb-4 bg-white-100 pt-4 pr-4 border-1 border-solid border-gray-100">
            <ul class="pl-8">
                @foreach($trendingThreads as $thread)
                    <li class="list-disc text-blue-500 hover:underline pb-2">
                        <a
                            class="text-gray-500"
                            href="/threads/{{$thread->channel->slug}}/{{$thread->slug}}">{{$thread->title}}
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endif
