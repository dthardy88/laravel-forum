@if(Auth::user())
    <usernotifications
        :notifications="{{auth()->user()->unreadNotifications}}">
    </usernotifications>
@endif
