<div id="search-bar">
    <div class="flex">
        <form
            method="GET"
            action="{{route('search.show')}}"
            class="flex">
            @csrf
            <input
                type="text"
                class="text-sm border-gray-100 text-gray-100 pl-2 mr-1"
                name="q"
                placeholder="Search for a thread..."/>
            <button class="fas fa-search bg-primary-100 p-2 text-white-100 rounded-none text-base hover:no-underline hover:bg-secondary-100"></button>
        </form>
        </form>
    </div>
</div>
