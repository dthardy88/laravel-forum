<div class="flex">
	<div class="flex-1">
		{{$heading}}
	</div>
	<div>
		{{$date}}
	</div>
</div>
<p class="py-4">
	{{$body}}
</p>