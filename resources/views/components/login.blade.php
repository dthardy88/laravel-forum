@if(Auth::user())
    <usernotifications
        :notifications="{{auth()->user()->unreadNotifications}}">
    </usernotifications>
@endif
<div class="flex sm:justify-end pt-2 sm:pt-0">
        <div class="flex">
            <i class="fas fa-power-off text-gray-300 text-xs mr-1 mt-1 flex self-center"></i>
            @if(Auth::user())
                <form
                    action="{{route('logout')}}"
                    method="post">
                    @csrf
                    <input
                        type="submit"
                        value="Logout"
                        class="cursor-pointer bg-transparent hover:underline">
                </form>
            @else
                <a href="{{route('login')}}" class="text-primary-100 hover:underline">Login</a>
            @endif
        </div>
</div>
