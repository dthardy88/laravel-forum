@extends('app')
@section('content')
<div id="channels-index" class="md:flex">
    <div class="w-full md:w-3/4">
        <div id="channel-list-header" class="w-full bg-primary-100 max-w-7xl text-white-100 px-2 py-4 mt-4 flex">
            <div class="w-2/5">
                <h2>Channels</h2>
            </div>
            <div class="w-1/5 hidden sm:block">
                Threads
            </div>
            <div class="w-2/5 hidden sm:block">
                Latest thread
            </div>
        </div>
        @foreach($channels as $channel)
            <div class="py-4 bg-white-100 border-b-1 border-gray-100">
                <div class="flex">
                    <div class="w-full sm:w-2/5 px-4 pb-6">
                        <h3 class="text-blue-500 hover:underline">
                            <a href="{{route('channel.show', $channel->slug)}}">
                                {{$channel->name}}
                            </a>
                        </h3>
                    </div>
                    <div class="w-1/5 hidden sm:block text-gray-400">
                        {{$channel->threads()->count()}}
                    </div>
                    <div class="w-2/5 hidden sm:block">
                        <a
                            href="{{route('threads.show', [$channel->slug, $channel->threads->first()->slug])}}"
                            class="text-primary-100 font-semibold hover:underline"
                        >
                            {{$channel->threads->first()->title}}
                        </a>
                        <p class="text-gray-400">By <u class="font-semibold">{{$channel->threads->first()->owner->username}}</u></p>
                        <p class="text-gray-400">{{$channel->threads->first()->created_at->format('D M Y H:i')}}</p>
                    </div>
                </div>
            </div>
        @endforeach
        <div id="pagination">
            <span class="pt-4 block">{{$channels->render()}}</span>
        </div>
    </div>
    <div id="trending-widget" class="w-full md:w-1/4 md:pl-4 mb-8 md:mb-0">
        @include('components.widgets.trending-threads')
    </div>
</div>

@include('components.widgets.statstics')

@endsection


