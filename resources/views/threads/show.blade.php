@extends('app')
@section('content')
	<thread :thread="{{$thread}}" inline-template>
		<div class="flex">
			<div class="w-3/4 p-4">
                <div>


                    <div
                        v-if="editing"
                        class="mb-4">
                        <input
                            type="text"
                            v-model="form.title"
                            class="w-full rounded-md border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 bg-gray-100 mb-4"
                        >
                        <textarea
                            rows="6"
                            v-model="form.body"
                            class="w-full rounded-md shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 bg-gray-100 mb-4">
                        </textarea>
                        <x-button
                            type="submit"
                            class="mb-4 bg-blue-500"
                            @click.prevent="updateThread">
                            Update
                        </x-button>
                        <x-button
                            type="submit"
                            class="mb-4 bg-gray-500"
                            @click.prevent="cancelUpdate">
                            Cancel
                        </x-button>
                    </div>


                    <div v-else>
                        <h1 class="text-xl" v-text="title"></h1>
                        <p v-text="body"></p>
                    </div>
                    <div class="replies">
                        <h2 class="text-lg">Replies</h2>
                        <replies
                            @newreply="repliesCount++"
                            @removereply="repliesCount--"
                        />
                    </div>
                </div>
			</div>
			<div class="w-1/4" id="sidebar">
                <div class="flex">
                    <div class="mr-4" v-if="$authorize('isAdmin')">
                        <x-button
                            type="submit"
                            class="mb-4 bg-blue-500"
                            @click.prevent="toggleLock"
                            v-text="locked ? 'Unlock thread' : 'Lock thread'">
                        </x-button>
                    </div>
                    <div>
                        @can('delete', $thread)
                            <form
                                action="{{route('threads.destroy', $thread->slug)}}"
                                method="POST">
                                @csrf
                                {{method_field('DELETE')}}
                                <x-button
                                    type="submit"
                                    class="mb-4 bg-red-800">
                                    {{__('Delete Thread')}}
                                </x-button>
                            </form>
                        @endcan
                    </div>
                </div>
                <div class="flex">
                    <div class="mr-4" v-if="$authorize('isAdmin')">
                        <div>
                            <div v-if="editing">
                                <x-button
                                    class="mb-4 mr-4 bg-green-500"
                                    @click.prevent="editing = true"
                                    disabled
                                >
                                    Edit Thread
                                </x-button>
                            </div>
                            <div v-if="$authorize('owns', {{$thread}})">
                                <div v-if="!editing">
                                    <x-button
                                        class="mb-4 mr-4 bg-green-500"
                                        @click.prevent="editing = true">
                                        Edit Thread
                                    </x-button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<hr />
				<div class="pt-4">
					<p>This thread was published {{$thread->created_at->diffForHumans()}} by <a href="{{route('user.profile', $thread->owner->username)}}" class="text-blue-900 hover:underline">{{$thread->owner->name}}</a>, and currently has <span v-text="repliesCount"></span> {{Str::plural('reply', $thread->replies_count)}}</p>
				</div>
				<subscribebtn
                    v-if="$authorize('signedIn')"
					:isactive="{{json_encode($thread->isSubscribedTo)}}">
				</subscribebtn>
			</div>
		</div>
	</thread>
@endsection


