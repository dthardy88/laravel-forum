@extends('app')
@section('content')
	<div class="w-3/4">
		<form
			action="{{route('threads.store')}}"
			method="POST">
			@csrf
			<x-label>Choose a Channel</x-label>
			<select
				name="channel_id"
				class="w-full rounded-md w-full shadow-sm border-gray-300 focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50 mb-4" blank_as_first required>
				<option value default>Choose One...</option>
				@foreach($channels as $channel)
					<option
						value="{{$channel->id}}"
						{{old('channel_id') == $channel->id ? 'selected' : ''}}>
						{{$channel->name}}
					</option>
				@endforeach
			</select>
			<x-label>Title</x-label>
			<x-input
				name="title"
				type="text"
				placeholder="Title"
				value="{{old('title')}}"
				required />
			<x-textarea
				placeholder="Have something to say?"
				name="body"></x-textarea>
            <div
                class="g-recaptcha mt-4"
                data-sitekey="6LdmWAcdAAAAAMt89aROqjDB7IyOIgp7WIuCaQaa">
            </div>
            <x-button class="mt-4">Publish</x-button>
		</form>
		@if(count($errors))
			<ul id="form-errors" class="mt-8 py-4 pl-8 bg-red-100">
				@foreach($errors->all() as $error)
					<li class="list-disc">{{$error}}</li>
				@endforeach
			</ul>
		@endif
	</div>
@endsection


