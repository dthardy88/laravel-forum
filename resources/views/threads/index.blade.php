@extends('app')
@section('content')
    <div id="thread-index" class="md:flex">
        <div class="w-full md:w-3/4">
            <div class="w-full bg-primary-100 max-w-7xl text-white-100 px-2 py-4 mt-4 flex">
                <div class="w-1/2">
                    <h2 class="font-light font-semibold">Threads</h2>
                </div>
                <div class="w-1/6 hidden sm:block">
                    Owner
                </div>
                <div class="w-1/6 hidden sm:block">
                    Visits
                </div>
                <div class="w-1/6 hidden sm:block">
                    Replies
                </div>
            </div>
            @foreach($threads as $thread)
                <article class="py-4 bg-white-100 border-b-1 border-gray-100">
                    <div class="flex">
                        <div class="w-full sm:w-1/2 px-4 pb-6">
                            <h3 class="text-blue-500 font-semibold hover:underline">
                                <a href="{{route('threads.show', [$thread->channel->slug, $thread->slug])}}">
                                    @if($thread->hasNewReplies())
                                        <strong>{{$thread->title}}</strong>
                                    @else
                                        {{$thread->title}}
                                    @endif
                                </a>
                            </h3>
                            <p class="body text-gray-400">{{substr($thread->body, 0, 75)}}...</p>
                            <div class="sm:hidden pt-4">
                                {{$thread->replies_count}} replies
                            </div>
                        </div>
                        <div class="w-1/6 hidden sm:block">
                            <p class="pr-4">
                                <a href="{{route('user.profile', $thread->owner->username)}}" class="text-blue-900 hover:underline">
                                    {{$thread->owner->name}}
                                </a>
                            </p>
                        </div>
                        <div class="hidden sm:block article-footer flex-1 w-1/6">
                            {{$thread->visits}}
                        </div>
                        <div class="w-1/6 hidden sm:block">
                            {{$thread->replies_count}} replies
                        </div>
                    </div>
                </article>
            @endforeach
            <span class="pt-4 block">{{$threads->render()}}</span>
        </div>
        <div class="w-full md:w-1/4 md:pl-4 mb-8 md:mb-0">
            @include('components.widgets.trending-threads')
        </div>
    </div>
@endsection


