let user = window.App.user;

module.exports = {
    owns(model, prop = 'user_id') {
        return model[prop] === user.id;
    },
    signedIn() {
        return window.App.signedIn
    },
    isAdmin() {
      return ['DBoy1988'].includes(user.username);
    },
}
